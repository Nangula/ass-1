import ballerina/grpc;
import ballerinax/mongodb;
import ballerina/io;

listener grpc:Listener ep = new (9090);

mongodb:ClientConfig mongoConfig = {
    host: "localhost",
    port: 27017,
    options: {sslEnabled: false, serverSelectionTimeout: 5000}
};

string database = "RepositoryFinction"; 
string collection_name = "FunctionsCollection";

mongodb:Client mongoClient = check new (mongoConfig,database);

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "RepoFunctions" on ep {

    remote function add_new_fn(fn value) returns responses|error {
        io:println(value);
        // converting fn record into map json
        map<json> new_fn = <map<json>> value.toJson();
        checkpanic mongoClient->insert(new_fn ,collection_name);
        responses res = {message: "function saved successfully."};
        return res;
    }
    remote function delete_fn(string value) returns responses|error {
        io:println(value);
        map<json> delete_fn_Filter = { "fn_key": value };
        responses res = {};
        int deleteRet = checkpanic mongoClient->delete(collection_name, (), delete_fn_Filter, true);
        if (deleteRet > 0 ) {
            // if greater than 1 when function updates
            res= {message:"Function deleted successfully"};
        } else {
            res= {message:"Function failed to delete"};
        }
        return res;
    }
    remote function show_fn(version_key value) returns fn|error {
            io:println(value);
            fn fun_res = {};
            map<json> fn_filter = <map<json>>value.toJson();
            map<json>[] showfns = checkpanic mongoClient->find(collection_name, (), fn_filter);        

            if(showfns.length()>0){
                map<json> fn1 = showfns[0];
                // removing Id before converting to Record fn
                var dt1 = fn1.remove("_id");
                fn|error fns = showfns[0].cloneWithType(fn);
                if (fns is error) {
                    io:println("Error Occured");
                }else{
                    fun_res = fns;
                }
            }

            return fun_res;
    }
    remote function add_fns(stream<fn, grpc:Error?> clientStream) returns responses|error {
        check clientStream.forEach(function(fn fn){
            io:println(fn);
            checkpanic mongoClient->insert(fn,collection_name);
        });

        responses res = { message: "Function successfully added."};
        return res;
    }
    remote function show_all_fns(string value) returns stream<fn, error?>|error {
            io:println(value);
            fn [] get_fns = []; 
            map<json> queryString = {"fn_key": value };
            map<json>[] allfns = checkpanic mongoClient->find(collection_name, (), queryString);
            foreach var data in allfns {
                json dt = data.remove("_id");
                fn|error fns = data.cloneWithType(fn);
                    io:println(fns);
                    if (fns is error) {
                        io:println("Error Occured");
                    }else {
                        get_fns.push(fns);
                    }
            }
            return get_fns.toStream();
    }
    remote function show_all_with_criteria(stream<string, grpc:Error?> clientStream) returns stream<fn, error?>|error {
        fn [] functions = [];
        map<json>[] fnsCriteria = checkpanic mongoClient->find(collection_name, (), ({}));
        check clientStream.forEach(function(string names){
            io:println(names);
            foreach var item in fnsCriteria {
                if (item.fn_metadata.language===names){
                    json fn1 = item.remove("_id");
                    json fn2 = item;
                    fn|error fns = fn2.cloneWithType(fn);
                    if (fns is error) {
                        io:println("Error Occured");
                    }else {
                        functions.push(fns);
                    }
                } 
                if (item.fn_metadata.keywords===names) {
                    json fn1 = item.remove("_id");
                    json fn2 = item;
                    fn|error fns = fn2.cloneWithType(fn);
                    if (fns is error) {
                        io:println("Error Occured");
                    }else {
                        functions.push(fns);
                    }
                }
            }
        });
        
        return functions.toStream();
    }
}

