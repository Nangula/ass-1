import ballerina/grpc;

public isolated client class RepoFunctionsClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function add_new_fn(fn|ContextFn req) returns (responses|grpc:Error) {
        map<string|string[]> headers = {};
        fn message;
        if (req is ContextFn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("RepoFunctions/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <responses>result;
    }

    isolated remote function add_new_fnContext(fn|ContextFn req) returns (ContextResponses|grpc:Error) {
        map<string|string[]> headers = {};
        fn message;
        if (req is ContextFn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("RepoFunctions/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <responses>result, headers: respHeaders};
    }

    isolated remote function delete_fn(string|ContextString req) returns (responses|grpc:Error) {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("RepoFunctions/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <responses>result;
    }

    isolated remote function delete_fnContext(string|ContextString req) returns (ContextResponses|grpc:Error) {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("RepoFunctions/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <responses>result, headers: respHeaders};
    }

    isolated remote function show_fn(version_key|ContextVersion_key req) returns (fn|grpc:Error) {
        map<string|string[]> headers = {};
        version_key message;
        if (req is ContextVersion_key) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("RepoFunctions/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <fn>result;
    }

    isolated remote function show_fnContext(version_key|ContextVersion_key req) returns (ContextFn|grpc:Error) {
        map<string|string[]> headers = {};
        version_key message;
        if (req is ContextVersion_key) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("RepoFunctions/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <fn>result, headers: respHeaders};
    }

    isolated remote function add_fns() returns (Add_fnsStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("RepoFunctions/add_fns");
        return new Add_fnsStreamingClient(sClient);
    }

    isolated remote function show_all_fns(string|ContextString req) returns stream<fn, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("RepoFunctions/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        FnStream outputStream = new FnStream(result);
        return new stream<fn, grpc:Error?>(outputStream);
    }

    isolated remote function show_all_fnsContext(string|ContextString req) returns ContextFnStream|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("RepoFunctions/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        FnStream outputStream = new FnStream(result);
        return {content: new stream<fn, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function show_all_with_criteria() returns (Show_all_with_criteriaStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("RepoFunctions/show_all_with_criteria");
        return new Show_all_with_criteriaStreamingClient(sClient);
    }
}

public client class Add_fnsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendFn(fn message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextFn(ContextFn message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveResponses() returns responses|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <responses>payload;
        }
    }

    isolated remote function receiveContextResponses() returns ContextResponses|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <responses>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class FnStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|fn value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|fn value;|} nextRecord = {value: <fn>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class Show_all_with_criteriaStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendString(string message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextString(ContextString message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveFn() returns fn|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <fn>payload;
        }
    }

    isolated remote function receiveContextFn() returns ContextFn|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <fn>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class RepoFunctionsFnCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendFn(fn response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextFn(ContextFn response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class RepoFunctionsResponsesCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendResponses(responses response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextResponses(ContextResponses response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextStringStream record {|
    stream<string, error?> content;
    map<string|string[]> headers;
|};

public type ContextFnStream record {|
    stream<fn, error?> content;
    map<string|string[]> headers;
|};

public type ContextString record {|
    string content;
    map<string|string[]> headers;
|};

public type ContextFn record {|
    fn content;
    map<string|string[]> headers;
|};

public type ContextResponses record {|
    responses content;
    map<string|string[]> headers;
|};

public type ContextVersion_key record {|
    version_key content;
    map<string|string[]> headers;
|};

public type fn_metadata record {|
    string language = "";
    Developer developer = {};
    string keywords = "";
|};

public type fn record {|
    string fn_content = "";
    string fn_key = "";
    fn_metadata fn_metadata = {};
    int fn_version = 0;
    string fn_name = "";
|};

public type Developer record {|
    string fullname = "";
    string email = "";
|};

public type responses record {|
    string message = "";
|};

public type version_key record {|
    int fn_version = 0;
    string fn_key = "";
|};

const string ROOT_DESCRIPTOR = "0A0A7265706F2E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F223D0A09446576656C6F706572121A0A0866756C6C6E616D65180120012809520866756C6C6E616D6512140A05656D61696C1802200128095205656D61696C226F0A0B666E5F6D65746164617461121A0A086C616E677561676518012001280952086C616E677561676512280A09646576656C6F70657218022001280B320A2E446576656C6F7065725209646576656C6F706572121A0A086B6579776F72647318032001280952086B6579776F72647322A1010A02666E121D0A0A666E5F636F6E74656E741801200128095209666E436F6E74656E7412150A06666E5F6B65791802200128095205666E4B6579122D0A0B666E5F6D6574616461746118032001280B320C2E666E5F6D65746164617461520A666E4D65746164617461121D0A0A666E5F76657273696F6E1805200128055209666E56657273696F6E12170A07666E5F6E616D651807200128095206666E4E616D6522250A09726573706F6E73657312180A076D65737361676518012001280952076D65737361676522430A0B76657273696F6E5F6B6579121D0A0A666E5F76657273696F6E1801200128055209666E56657273696F6E12150A06666E5F6B65791802200128095205666E4B65793297020A0D5265706F46756E6374696F6E73121D0A0A6164645F6E65775F666E12032E666E1A0A2E726573706F6E736573121C0A076164645F666E7312032E666E1A0A2E726573706F6E736573280112350A0964656C6574655F666E121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A0A2E726573706F6E736573121C0A0773686F775F666E120C2E76657273696F6E5F6B65791A032E666E12330A0C73686F775F616C6C5F666E73121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A032E666E3001123F0A1673686F775F616C6C5F776974685F6372697465726961121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A032E666E28013001620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "repo.proto": "0A0A7265706F2E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F223D0A09446576656C6F706572121A0A0866756C6C6E616D65180120012809520866756C6C6E616D6512140A05656D61696C1802200128095205656D61696C226F0A0B666E5F6D65746164617461121A0A086C616E677561676518012001280952086C616E677561676512280A09646576656C6F70657218022001280B320A2E446576656C6F7065725209646576656C6F706572121A0A086B6579776F72647318032001280952086B6579776F72647322A1010A02666E121D0A0A666E5F636F6E74656E741801200128095209666E436F6E74656E7412150A06666E5F6B65791802200128095205666E4B6579122D0A0B666E5F6D6574616461746118032001280B320C2E666E5F6D65746164617461520A666E4D65746164617461121D0A0A666E5F76657273696F6E1805200128055209666E56657273696F6E12170A07666E5F6E616D651807200128095206666E4E616D6522250A09726573706F6E73657312180A076D65737361676518012001280952076D65737361676522430A0B76657273696F6E5F6B6579121D0A0A666E5F76657273696F6E1801200128055209666E56657273696F6E12150A06666E5F6B65791802200128095205666E4B65793297020A0D5265706F46756E6374696F6E73121D0A0A6164645F6E65775F666E12032E666E1A0A2E726573706F6E736573121C0A076164645F666E7312032E666E1A0A2E726573706F6E736573280112350A0964656C6574655F666E121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A0A2E726573706F6E736573121C0A0773686F775F666E120C2E76657273696F6E5F6B65791A032E666E12330A0C73686F775F616C6C5F666E73121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A032E666E3001123F0A1673686F775F616C6C5F776974685F6372697465726961121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A032E666E28013001620670726F746F33"};
}

