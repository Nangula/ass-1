import ballerina/io;
import ballerina/grpc;
RepoFunctionsClient ep = check new ("http://localhost:9090");

public function main() {
    io:println("Developer Repo Function ");
    io:println("-----------------------------------------");
    io:println("1. Add New Function");
    io:println("2. Add multiple Functions");
    io:println("3. Delete Function");
    io:println("4. Show Function");
    io:println("5. Show All Functions");
    io:println("6. Show all Functions with Criteria");
    io:println("----------------------------------------");
    string choose = io:readln("Choose an option: ");

    if (choose === "1"){
        add_new_fn();
    }else if (choose === "2") {
        error? fns = add_fns();
        if fns is error {
            io:println(fns);
        }
    }else if (choose === "3") {
        delete_fn();
    }else if (choose === "4") {
        show_fn();
    }else if (choose === "5") {
        error? showAllfns = show_all_fns();
        if showAllfns is error {
            io:println("Error Occured");
        }
    }else if (choose === "6") {
        error? criteria = show_all_with_criteria();
        if (criteria is error){
            io:println("Error Criteria ...");
        }
    }
}


public function add_new_fn() {
    io:println("-------- New Functions ----------");
    fn fns = {
        fn_content: "public function main(){ io:println(hellena)}",
        fn_key:"hellena",
        fn_metadata: {
            developer: {
                email: "hellena&mail.com",
                fullname: "Hellena Hellena"
                },
                keywords: "ballerina is awesome",
                language: "Ballerina"
            },
        fn_version: 0
    };
    var res = ep->add_new_fn(fns);
    if res is grpc:Error {
        io:println(res.toString());
    } else {
        io:println(res);
    }
    io:println("--------------------------------------");
    string back = io:readln("Click 1 to go back: ");
    var back1 = (back === "1")?main():main();
}

public function add_fns() returns error?{
    io:println("--------create multiple Functions----------");
    fn [] gt = [];

    fn fns = {
        fn_content: "public function main(){ io:println(hellena)}",
        fn_key:"hellena",
        fn_metadata: {
            developer: {
                email: "hellena&mail.com",
                fullname: "Hellena Hellena"
                },
                keywords: "ballerina is awesome",
                language: "Ballerina"
            },
        fn_version: 0
    };

    fn fns1 = {
        fn_content: "public function main(){ io:println(hellena)}",
        fn_key:"hellena",
        fn_metadata: {
            developer: {
                email: "hellena&mail.com",
                fullname: "Hellena Hellena"
                },
                keywords: "ballerina is awesome",
                language: "Ballerina"
            },
        fn_version: 0
    };

    fn fns2 = {
        fn_content: "public function main(){ io:println(hellena)}",
        fn_key:"hellena",
        fn_metadata: {
            developer: {
                email: "hellena&mail.com",
                fullname: "Hellena Hellena"
                },
                keywords: "ballerina is awesome",
                language: "Ballerina"
            },
        fn_version: 0
    };

    gt.push(fns);
    gt.push(fns1);
    gt.push(fns2);  

    Add_fnsStreamingClient streamclient = check ep->add_fns();

    foreach fn item in gt {
        check streamclient->sendFn(item);
    }

    check streamclient->complete();

    json? response = check streamclient->receiveResponses();
    io:println(response);
    
    io:println("--------------------------------------");
    string back = io:readln("Click 1 to go back: ");
    var back1 = (back === "1")?main():main();
}

public function delete_fn() {
    io:println("-----------Deleting fns----------------");
    var respond =  ep->delete_fn("hellena");
    if respond is grpc:Error {
        io:println(respond.toString());
    } else {
        io:println(respond);
    }
    io:println("--------------------------------------");
    string back = io:readln("Click 1 to go back: ");
    var back1 = (back === "1")?main():main();

}

public function show_fn() {
    io:println("------Show Functions with version--------");
    version_key versn_key = {fn_version:2,fn_key:"hellena"};
    var response =  ep->show_fn(versn_key);
    if response is grpc:Error {
        io:println(response.toString());
    } else {
        io:println(response);
    }
    io:println("--------------------------------------");
    string back = io:readln("Click 1 to go back: ");
    var back1 = (back === "1")?main():main();

}

public function show_all_fns() returns error? {
    io:println("--------Show All Fns Latest Versions------");
    var respond =  ep->show_all_fns("hellena");
    if respond is grpc:Error {
        io:println(respond.toString());
    } else {
        check respond.forEach(function(fn fn){
            io:print(io:println(fn));
        });
    }
    io:println("-------------------------------------------");
    string back = io:readln("Click 1 to go back: ");

    var back1 = (back === "1")?main():main();

}

public function show_all_with_criteria() returns error?{
    io:println("--------------Show All with Criteria-----------------");
    string [] criteria = ["Ballerina","Java","Javascript","Python"];
    Show_all_with_criteriaStreamingClient showCriteria = check ep->show_all_with_criteria();

    foreach string item in criteria {
        check showCriteria->sendString(item);
    }
    check showCriteria->complete();

    fn | grpc:Error? fns = check showCriteria->receiveFn();
    while !(fns is ()) {
        io:println(fns);
        fns = check showCriteria->receiveFn();
    }
    io:println("----------------------------------------------------");
    string back = io:readln("Click 1 to go back: ");
    var back1 = (back === "1")?main():main();

}


